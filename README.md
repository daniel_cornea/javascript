# README #

This README documents the steps necessary to get the webpages up and running.

### What is this repository for? ###

* Quick summary
This repo contains three projects that I worked on during the courses at 42.
 
-----------1st project "The balloon"--------------------

Contains a circle, if the user clicks on circle the circle expands and changes color, if the user moves the mouse from the interior of the circle to the exterior, the circle will shrink. 

-----------2nd project "Calc"---------------------------

It is a simple calculator that does various operations using the 'select' form.
Press the button "Try me" to perform the operations. Also, if you do not use the calc for more than 30 seconds it will ask you to use it. 

----------3rd project "To do list"----------------------

It is a simple implementation of a to do list. 

## How do I get set up? ###
I used Bitnami to get my local Apache server up and running, you are free to choose whatever application you want, just make sure to copy the projects in the "htdocs" folder (or equivalent) and access the projects via localhost from browser (I used Google Chrome).


### Who do I talk to? ###

* Daniel Cornea 
* danok.cornea@gmail.com